'''pool_simple_map_apply.py'''
import multiprocessing as mp

def cube(x):
      return x**3


if __name__ == '__main__':
    p = mp.Pool(processes=4)
    results = p.map(cube, range(1,7))
    print(results) #[1, 8, 27, 64, 125, 216]



    p= mp.Pool(processes=4)
    results = [p.apply(cube, args=(x,)) for x in range(1,7)]
    print(results) #[1, 8, 27, 64, 125, 216]
