from intensive import busy_sleep
from multiprocessing import Process

N = 100000000

p1 = Process(target=busy_sleep, args=(N//2, ))
p2 = Process(target=busy_sleep, args=(N//2, ))
p1.start()
p2.start()
p1.join()
p2.join()
