"""daemon.py"""
from multiprocessing import Process, current_process
import time

def non_daemon(num):
    print( "in non_daemon block ", current_process().name)

def daemon(num):
    time.sleep(4)
    print( "in daemon block ", current_process().name)

if __name__ == '__main__':

    p1 = Process(name ='worker1', target=non_daemon, args=(1,))
    p2 = Process(name ='service', target=daemon, args=(2,))
    p2.daemon =True
    p1.start()
    p2.start()
    time.sleep(1)
    #p2.join()
    print( "All finished")
