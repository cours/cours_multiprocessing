### Sommaire
- Introduction <!-- .element: class="fragment" data-fragment-index="1" -->
  - multithreading <!-- .element: class="fragment" data-fragment-index="2" -->
  - GIL <!-- .element: class="fragment" data-fragment-index="2" -->
- Librairie Multiprocessing  <!-- .element: class="fragment" data-fragment-index="3" -->
  - Process, Queue, Pool   <!-- .element: class="fragment" data-fragment-index="4" -->
  - Exemples  <!-- .element: class="fragment" data-fragment-index="5" -->
  - Exercices   <!-- .element: class="fragment" data-fragment-index="6" -->
