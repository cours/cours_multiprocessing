#### La classe <span style="color:red" >*Pool*</span>

- *Pool* est une classe que permet de paralléliser facilement.
- `p = Pool(processes=4)`  est un ensemble de 4 processus
  sur lequel une fonction peut être appliquée.

- `p.map` permet de "mapper" la fonction:
  ```python
  '''pool_simple_map_apply.py'''
  import multiprocessing as mp

  def cube(x):
      return x**3

  p = Pool(processes=4)
  results = p.map(cube, range(1,7))
  print(results) #[1, 8, 27, 64, 125, 216]
  ```
- `p.apply` est utile pour les fonctions avec plusieurs arguments:
  ```python
  p= Pool(processes=4)
  results = [p.apply(cube, args=(x,)) for x in range(1,7)]
  print(results) #[1, 8, 27, 64, 125, 216]
  ```
- Il exist d'autres fonctions :
  `p.imap`, `p.imap_unordered`, `p.map_async`, `p.starmap`
