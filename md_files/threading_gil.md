### Quelques problème avec *threading*

Considérons la fonction intensive (CPU-bound):

```python
"""python/intensive.py"""
def busy_sleep(n):
    while n > 0:
        n -= 1
```
Avec un seul thread:

```python
"""python/seq_gil.py"""
from intensive import busy_sleep
N = 100000000
busy_sleep(N)
```
