#### La classe <span style="color:red" >*Queue*</span> : Exercice

Céer un systeme avec double queue:

- Une queue utilisée pour amener des valeurs dans les worker (id).
- Chaque processus ajoutera une `str` avec son PID à le valeur initial.
- Une queue utilisée pour récupérer les résultats.
