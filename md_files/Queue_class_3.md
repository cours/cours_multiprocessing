#### La classe <span style="color:red" >*Queue*</span> : Exercice

```python
import multiprocessing as mp
def add_pid(queue_in,queue_out):
    num = str(mp.current_process().pid)
    val = queue_in.get()
    queue_out.put(num +' ' + val)

if __name__ == "__main__":
    queue0 = mp.Queue()
    queue1 = mp.Queue()

    # fullfil the input queue
    [queue0.put(str(ii)) for ii in range(4)]

    # initialize process
    processes = [mp.Process(target=add_pid, args=(queue0,queue1,)) for x in range(4)]

    for p in processes:
        p.start()

    for p in processes:
        p.join()

    # get results from output queue
    results = [ queue1.get() for p in processes ]
    print (results)

```
