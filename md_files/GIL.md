## GIL : Global Interpreter Lock
Un feu rouge : un seul thread à la fois accède à l'interpréteur.

Les threads sont serializés (exceptions: I/O, numpy,..)


<img class="center" src="/images/GIL.png" width="500px"></img>

#### <span style="color:red">Aucune performance dans le cas de multithreading </span>

https://docs.python.org/3/c-api/init.html
