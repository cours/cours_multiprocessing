```bash
python montecarlo_integration.py

Test integration of the function :
             1/(1+x**2)
Exact integration = 0.785398163397448
---------------------------------
Result for N = 1000000
 parallel   integration = 0.7853487146516074
 sequential integration = 0.785393457099523
Timing:
 sequential              = 1.8331952095031738
 parallel with 16 procs  = 0.34990477561950684
---------------------------------
Result for N = 10000000
 parallel   integration = 0.7852551714744861
 sequential integration = 0.7853972742304417
Timing:
 sequential              = 18.81016755104065
 parallel with 16 procs  = 1.770831823348999
---------------------------------
Result for N = 50000000
 parallel   integration = 0.7854155342916894
 sequential integration = 0.7853978153100228
Timing:
 sequential              = 89.51818585395813
 parallel with 16 procs  = 7.661694526672363
```
