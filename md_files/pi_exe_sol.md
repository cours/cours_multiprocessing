#### La classe <span style="color:red" >*Pool*</span> : Exercice (Calcul de Pi en parallel)
Solution :

```python
'''pi_base.py'''
import multiprocessing
import math
import time

def partial_pi(izero, iend, nb):
    ppi = 0
    nb_inv = 1. / nb
    for ii in range(izero,iend):
        jj = (ii + 0.5) * nb_inv
        ppi += 1./(1. + jj**2)
    return ppi

def partial_pi_wrap(args):
    return partial_pi(*args)

```
