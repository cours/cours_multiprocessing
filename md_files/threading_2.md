### *threading*  (Exemple)
`cat python/threading_0.py`

```python
import threading

def worker(num):
    """thread worker function"""
    print 'Worker: %s' % num
    return

threads = []
for i in range(5):
    t = threading.Thread(target=worker, args=(i,))
    threads.append(t)
    t.start()
```


```bash
python python/threading_0.py
Worker: 0
Worker: 1
Worker: 2
Worker: 3
Worker: 4
```
<!-- .element: class="fragment" data-fragment-index="1" -->
