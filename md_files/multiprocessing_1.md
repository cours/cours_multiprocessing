### *multiprocessing*

- Semblable à *threading* mais basée sûr les processus
- Countournement du GIL
- API de niveau encore plus élevée
- classe de base `Process`

#### On verra:
- `Queue`
- `Pool`

#### On verra pas:
- `Pipe`
- `Lock`
- `Shared ctypes Objets`


https://docs.python.org/3/library/multiprocessing.html
