#### La classe <span style="color:red" >*Pool*</span> : Exercice (Calcul de Pi en parallel)
Solution :

```python
if __name__ == '__main__':
    nb = 100000000 #int(input("number of bins : "))
    nproc = mp.cpu_count()//2

    start = time.time()
    div   = nb // nproc
    if nb % nproc != 0 : div += 1
    nb = nproc * div
    pool = mp.Pool(nproc)

    # using imap_unordered
    lista = ((ii*div,(ii+1)*div,nb)  for ii in range(nproc))
    result = pool.imap_unordered(partial_pi_wrap,lista)
    ppi = 4./nb * sum(result)
    end = time.time()

    print("Valeur approchee de pi : {} (delta={:e})".format(ppi,abs(ppi-math.pi)))
    print("Calculee en {} par {} processus".format(end-start,nproc))

```


Une solution alternative :
<!-- .element: class="fragment" data-fragment-index="1" -->
```
        # using Apply does not work
        lista = ((ii*div,(ii+1)*div)  for ii in range(nproc))
        result = (pool.apply(partial_pi, args=(x1,x2,nb)) for x1,x2 in lista)

```
<!-- .element: class="fragment" data-fragment-index="1" -->
