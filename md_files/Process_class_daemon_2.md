#### La classe <span style="color:red" >*Process*</span> : Exemple (Demon)
Avec `sleep(4)` dans le daemon:

```bash
python daemon.py
in non_daemon block  worker1
All finished

```

Avec `sleep(4)` dans le non_daemon:

```bash
python daemon.py
in daemon block  service
All finished
in non_daemon block  worker1
```
