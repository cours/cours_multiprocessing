#### La classe <span style="color:red" >*Queue*</span>

*Queue* sert à échanger des données (et des objets) entre les processus (FIFO structure).

`q = Queue()`




- `q.empty()` si la queue est vide
- `q.get()` récupère et vide la queue de son contenu
- `q.put(item)` met des données dans la queue
