#### La classe <span style="color:red" >*Pool*</span> : Exemple (integration Montecarlo)

Code pour le calcul de l'intégral

```python
def integral(function, xbound, numPoints=1000000):
    xmin, xmax = xbound
    ymin, ymax = find_yminymax(function, xbound, numPoints)
    rectArea = (xmax - xmin) * (ymax - ymin)
    ctr = 0
    for j in range(numPoints):
        x = xmin + (xmax - xmin) * random.random()
        y = ymin + (ymax - ymin) * random.random()
        if math.fabs(y) <= math.fabs(function(x)):
            if function(x) > 0 and y > 0 and y <= function(x):
                ctr += 1 # area over x-axis is positive
            if function(x) < 0 and y < 0 and y >= function(x):
                ctr -= 1 # area under x-axis is negative

    baseArea = ymin * (xmax - xmin)
    fnArea = rectArea * float(ctr) / numPoints
    return fnArea + baseArea
```
