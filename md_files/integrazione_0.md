#### La classe <span style="color:red" >*Pool*</span> : Exemple (integration Montecarlo)

On suppose de vouloir faire une intégration de type statistique :

<img  src="/images/montecarlo.png" width="300px"></img>  

En lançant un numéro important de points sur la courbe d'une fonction on peut calculer l'intégral de la fonction.
