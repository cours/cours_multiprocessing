#### La classe <span style="color:red" >*Process*</span> : Un Exercice tout simple


Créer n processus qui lancent un worker.

Le worker fait un print du nom du processus et de son PID.





Utiliser `multiprocessing.current_process` pour récupérer les
informations sur le processus:

`pid, _parent_pid, name`
