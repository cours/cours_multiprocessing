#### La classe <span style="color:red" >*Process*</span>

*Process* représente un processus:

`p = Process(target=f, args=('bob',))`




- *target* : associe une fonction au processus
- *args* : la list des arguments dans une n-tuple
- `p.start()`  lance le processus
- `p.join([timeout])`   attend son arret
- `p.name` nom du processus
- ... `pid, daemon, is_alive(), terminate() `
